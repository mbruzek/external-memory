# Lessons Learned hosting 1800 customer instances in 8 K8s Clusters

Bob Walker | Field CTO

Octopus Cloud

Deploying software. Client required deploying at 2am on Saturday.

bob.walker@octopus.com

## Brownfield migration

Cloud v1: each customer's instance is running on a windows ec2 instance
Cloud v2: each customer's instance is running in a k8s container

## Octopus deploy

Deploy container
SQL server
File storage
Networking/Load balancer

## Cloud v1

Why host everyone on EC2 instances?  
Been around since 2012. We would give them MSI to host on their VMs.  
Helping customers run Octopus deploy on their own systems.

Offer a SaaS solution. Customer request  
Octopus cloud v1 launched in 2018
First AWS bill almost killed them.

Lesson #1 Cost should not be the only factor, but should be considered

## Additional downsides

* harder to incrementally increase CPU/RAM resources
* no customer instance was utilized at 100%

## Kubernetes appeal

* initially considered vms and serverless
  * vms do not scale
  * serverless would require a complete rewrite

* kubernetes benefits
  * increased customer density
  * incremental scaling
  * desired state/immutability
  * upgrading instances

Building AMIs just like containers. 

## Migration to kubernetes

Announcing the general availability of windows server containers and private clusters for azure kubernetes services

Windows containers did not work in 2018 or 2019

## Linux container migration 

Fall 2018

Lesson # 2 The migration to containers will take longer than expected

## Port to Linux
* Moving to .NET core
  * Update or replace third party libraries
  * Replace all registry references
  * verify all filesystem functionality
  * find and fix all bugs in code and .NET runtime
* Fall 2018 -> Fall 2020

Build the plane while we are flying it
feature work, bug fixes, and patching did not stop

## Where to host SQL sever and file storage?
Use Cloud manage services
* Leverage their expertise
* It is already built
* It is their focus and constantly backed up

Run a script step is the most popular step.
Run powershell scripts 
VMs to run scripts on

## Not all managed services are the same

Migrate to Azure, running into AWS scalability issues
Every customer gets own database.

Use Azure SQL, elastic SQL pool. Everyone gets own database, but share resources.

Each customer is host in region around the world

kubernetes cluster info
* VMs - 3 to 6 nodes
* E20s_v4 - 20 vCPUs/160GB RAM
* 300 to 500 instances at scale

Now 1800 customers

Use third party tooling for functionality outside your company's core competency

Variety of use cases

Cloud Deployments

Preventing noisy neighbors
* Leverage Azure and AKS each instance to allocate dedicated ram/cpu

Monitoring and alerting 

Leverage custom tooling for your specific business requirements

Wrote custom software, Cloud Portal. Only accessible by employees.
* One stop shop for all things octopus related

Customer maintenance window

Billing system events
* What to do with customer non-payment
  * deactivate
  * archive

bob.walker@octopus.com

---  
