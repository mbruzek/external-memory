# 2024 TexasLinuxFest - Day 1

Cloud Native Computing Foundation (CNCF)

Chris Anisczyk

Formerly at Twitter Mesos project

## Future of Cloud Native (and open source)

The CNCF founded in 2015. 

Hosts 180+ projects  
230K+ contributors  
190 countries  

## Cloud Native history 101

2000 - non virtualized hardware  
2001 - Virtualization  
2006 - IaaS  
2009 - PaaS  
2010 - Opensource IaaS  
Opensource PaaS
Containers
Cloud Native

March toward multi-vendor multi hardware open source options for the whole software industry.

The CNCF history

## 2015 

Beginning of the CNCF

container image registry
container image repository
container runtime
Compute node OS
infrastructure provisioning

## 2016

Prometheus added!
Monitor the containers

## 2017

Landscape + Principles

## 2018

Sandbox 
cloud native definition
etcd

Knative is open source to build open source enterprise level serverless

## 2019

CNCF SIGs proposal 
CNCF Security SIG approved

K3S project created, lightweight distribution 

OpenTelemtry = Merger of open tracing and open census.

## 2020 

OpenTelemtry starts to grow

## 2021

eBPF Maturity 

eBPF = rans sandboxed programs in a privileged context such as operating system kernel. It is used to safely and efficiently extend the capabilities of the kernel without requiring ... (modules?)

## 2022

Wasm innovation = Web assembly

Wasm matured

https://wasi.dev   
https://github.com/containerd/runwasi

## 2023

CNCF Reaches 150+ projects

## 2024 

Infrastructure Provisioning
Compute Storage Network
Container-optimized OS
Container runtime - Container native storage - Container native network
Security & Hardening - Service Mesh

## CNCF Projects Evolve with the computer

Sandbox, incubating,  

## Future of cloud native

Backstage 
OpenTelemetry
OpenGitOps
OpenCost

---  

# Innovation in licensing

Andy from ControlPlane - Git handle @sublimino

Training Hashicorp, SANS, Linux Foundation, Docker

CISO for OpenUK

kubesec - security tool for kubernetes platforms
netassert - nmap worm to test networks
Bad Robot - static analysis for rbac
Simulator - CTF platform, kubecon, etc
Flux CD - Open source CD tool. Hardened SSL sell access to immediate packaging

## What it is (and is not) open source?

The foundation of modern tech development.  
Harnessing of the industry's collective intelligence  

Properties of OSI OSS = open source software
* Available for extension/patching  
* Distributed with source code  
* Unwarranted  
* Non-discriminatory  
* Free (freedom, beer)  

What is not OSS?  
* Project trademarks  
* Supporting the project's users  
* Hosting the project  
* Governance and distribution  
* Getting paid  

## Today's problems

* getting paid
* getting forked
* staying maintained
* staying employed

Hashicorp bait and switch

Cease and desist letter

MPL-2.0 -> BUSL-1.1 

## Sustainability and protection

Open source is not a business model
It is a production model

* balancing open contributions with license obligations
* The victory and cautionary tails of OSS adoption

## Economies of hyperscale

Big clouds, datacenters, host open source software at scale

"Open source ... is typically less convenient than service based alternatives" 

"GPL is 'squeezed' in the age of cloud" Peter Zaitsev CEO Percona

MIT/BSD licenses are essentially a donation to the public, including Amazon, Apple, Microsoft and Google who don't need donations. - Stefano Maffulli (OSI director)

## Sustainability how ot win at OSS

* Build a valuable thing
* register trademarks, domains, and socials
* invite sponsorship
* live stream progress
* ???
* Venture Capital

## Challenges to Profitability

* One in a million OSS projects get "Hyperscaler-Hosted"
* Ut is probably making a lot of money already
* Be so good that they can not ignore you
  * list of marketplaces
  * provide support and extendability
  * Partner for professional services

## Sustainability OSS business

* focus on financials
* The best money is customers money
* sales and marketing are either organic or heavily-funded
* Small teams may be able to do things better
  * Commercial stage teams risk esoteric feature delivery for niche customers

Discussion of Threat modeling Open Source

---  
