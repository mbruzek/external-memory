# Past, present future of Gateway API

Shane Utt at Kong
Gateway API maintainers

Current users of Ingress
Those that are new to or want to contribute

Working on AI/ML

Building ingress controller. Worked on kubernetes distribution.
Kong has supported his open source involvement.

# SIG Network

Work on core Kubernetes networking
* ingress API
* service API
* kube-proxy

 
## Who is Gateway API

The successor to the Ingress API

Meant to be highly extensible compared to Ingress API

Initial working group 5 years ago at KubeCon San Diego

What problem are we trying to solve?

Ingress in wide adoption, has a couple dozen implementations.
* Generally insufficient for most use cases
* Annotations and CRDs created portability issues
* No roles and limited permissions model  

Upstream is slowing down for new features  
Conformacne testing 

## Goals

Generic  
Expressive  
Extensible  
Role-oriented - separate concerns, access controls, security

## Key decisions

* Upstream but based on CRDs
* Gateway Enhancement Proposals (GEPs) instead of KEPS
* Role-oriented: Gateways became their own resource
* Release channels
* Conformance levels
* Conformance profiles

## What is Gateway API?

Next generation of kubernetes routing and load balancing APIs  
Where the next features are meant to land

Heavily focused on L7 (HttpRouting, GRPCRoute) today.  
Experimented with L4.

New feature "weights" on routes. Can do canary deployments.

GRPC route?

---  
