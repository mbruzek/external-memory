# Bash programming

Workshop hosted at https://bash.codrcodz.io

Cody Lee Cochran @codrcodz on gitlab.com

Bash resources: https://bash.codrcodz.io/resources/

The labs require the latest 5.2 version of bash.

Texas Linux Fest wifi only for the cameras, use Palmer Event Center wireless.

There is a lab may not be able to take great notes.

"It has been 30 minutes since my last bash script" - Cody Cochran

Work for a small defense contractor in Austin.

Share Bash stuff with everyone here.

## Tooling
* shdoc is an autodocs generator for Bash
* shfmt is an autoformatter for Bash
* shellcheck is a linter for Bash
* shunit2 is a unit test framework for Bash
* bashate is a PEP8 equivalent linter for Bash
* kcov code coverage tool that supports Bash

Great talk, I learned a lot!

vscodium is a telemetry-less code editor  

---