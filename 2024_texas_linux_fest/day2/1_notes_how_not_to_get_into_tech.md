# How (Not) To Get Into Tech

Anita Zhang from Meta

Open source speaker. Talks low level Linux stuff. Search for them later.

Software engineer manager at Meta.
Software engineer at Meta for 6 years.

Identity outside of employer. Dog owner. Bella adopted from shelter.

Linux enthusiast. Fan of anime.

Women are still in the minority, and Asian women is even more rare.

Did not think she would be in tech. Other engineers seem to have been working toward tech their whole lives.

First job was working in lab on Clusters. All got new cables. Bought a Thinkpad. Forgot the BIOS password.
Short pins to reset. 

Went overboard on slimming down everything in Linux. Forgot how to use the system she customized.

Now uses Fedora. Shout out to team Fedora.

Carnegie Mellon great CS program

Born and raised in San Francisco tight Chinese community.

Parents wanted her to be a pharmacist.

Rice, Fish, veggies for every meal. Low income lunch program at school.

Enjoy what you have.

Parents gave her a HP desktop. Slow but she fixed it. Had fun taking apart and fixing it.

Mom had a stereotypical view on things. Boys build things, girls housework. 

In highschool someone would help students apply for grants and scholarships. Only a few would apply.

Parents were 1st generation immigrants would not be able to help. Carnegie Mellon scholarship.

Wrote essay. Loved video games. Game academy. Take classes and hear from industry professionals. Playable demo end result.

Did not join the software development track. Joined the Art track. Loved to draw. 3D modeling.

Senior year of highschool. Carnegie Mellon would require to pre-select a major. Selected the college of engineering.

Very first experience with Linux. Forced to become a Linux user all machines were running Linux at college.

Only learned the bare minimum to get by. Didn't really get into Linux later, first distribution was Debian Squeeze.

Excluded Ubuntu because didn't want to make life easy. Learning experience.

Minimal is good, don't use heavy weight desktop environment. Fluxbox. Daily driver.

Learned about packages, debian stable is not fun, debian unstable has all the cool stuff. 

OS course was known to be difficult at Carnegie. Had to write OS with switching and complicated stuff.

Loved debugging, race conditions, etc. 

Course that comes before OS. Intro to computer systems. Prerequisite. Dropped out of the first time.

Jessica advisor helped her. Focused on the immigration aspect. 

Social anxiety. For speaking non-standard Cantonese dialect. Disservice that the college did not show the CS courses as more interesting. Become a teaching assistant. Took liberties with material to put own twist on things. Worked out well was a teaching assistant for next years.

Taught a course and was called "Professor Zhang". Wrote the TA grading software in Perl. They still used it for few years.

Recruiter found her resume in book somewhere. Wanted to do a internship with Meta to pursue masters degree. Why did you want to work at facebook? Did not get an internship offer. Less structured at the company that long ago.

I don't know if I want a fulltime offer. Luckily it worked out.

Security background and interest in low level systems. Came back full time, worked on some really interesting projects.

Known for the certificate authority wrangler. Wrote a production service from scratch. TLS certificates. Needed a service to scale to sign all the certificates. Lucky to be on the team that wrote the service. Operational a production method. Unlocked a lot of opportunities to work on other security related issues.

Risk of being fired. IC3 = Individual Contributor. There was a dead line to get to IC4. Stuck trying to get to IC5. In yellow zone more than 50% of the way. In the danger zone, manager was helping me as much as possible. 

Root of problem was not learning any more. Lucky break. Workspace news feed. Looking for someone to work on systemd. Containers team was looking for systemd. Looking for someone to provide Meta support to the systemd project.

Only interested in security and low level systems. Hack a month. Internal mobility program at Meta. To spend time with another team. To try something different. If it didn't work out could go back to another team.

It had been a few years since used GitHub. High risk, a crash could kill system. Dopamine hit when I debugged an issue.

Stage manager for Brodiecon. Fan convention for the show "My Little Pony". Adult male fans. Knew a lot of adult males. Baltimore MD. How ended up stage managing. Was into art. One of her pieces was on display. Thought she would be a tech for the high school. 

Randy Pauch lecture. Talked about living dreams, and getting over brick walls. 

Worked every position in the AV tech. Still volunteer for conventions today. 

Never wanted to become manager. Never say never. Manager solve people problems, deal with corporate politics. Soft skills I didn't want to make use of. I did go through personal growth. Manager asked to transition to management track. Heck no! Think about it longer and sleep on it. After a few months, made the transition. Manager supported open source. Pay forward with her own engineers.

Two years manager so far. Every manager has horror stories. First round of layoffs ever. Emotions. Team. Misconception, sometimes people would be forcibly transferred. Adaptability worked.

Would not have become a manager to support a team she didn't know. But she knew the people. Motivated by the project and the technology. 

Mom has stopped commenting on lack of women in her classes. Better now. One of a handful of women.

Lessons:  

* Life is a ride, enjoy it. 
* Never turn down an opportunity. 
* Pay it forward. 
* Don't take for granted what they did for you.

Friendship is magic. Be excellent to each other.

---  
