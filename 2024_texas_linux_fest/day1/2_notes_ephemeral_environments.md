# Ephemeral environments

## A Few things we are behind on

* Testing still happens in production (because it _has_ to)
  * This is inherently bad!
* Not enough staging/test environments to go around
* key stakeholders

## What would happen if you could have unlimited staging environments?

What kind of time would that save?

## Development with static long-lived environments

develop -> pull request -> merged to main -> deploy to staging -> test on staging -> deploy to production

Staging doesn't have same compute, storage, etc can't find all the bugs

## The right way ephemeral environments

* short-lived stand alone deployments of an app
* nearly identical to production (including integrations, database, etc)
* on demand
* secure (isolated from other environments)

## Development with ephemeral environments

develop -> pull request -> ephemeral environment (test suite) -> merge to main -> deploy to staging -> deploy to production

Ephemeral environments will trigger full test suite. Run tests until they pass.
Developers should not waste time reviewing code that is not tested.

## How do you build this?

* Use GitOps (argo, flux)
* Use containers where possible
* Use kubernetes
  * scale to zero
  * on demand
  * efficient use of resources
* Monitoring and observability
  * Opentelemetry, loki, prometheus

http://ephermeralenvironments.io

shipyard 

Static staging environments are not taught in school.  
First merge broke the staging environment.  

Don't be afraid to break things. Ephemeral environments are disposable.

## Ephemeral 101

1. collaboration - share environments across teams faster dev cycle
2. testing - automatic tests, full suite
3. security - isolated environments 
4. dev workflow - keeps your product up to date new code changes
5. cost control - cut costs with environments that scale to your needs
6. on demand - dodge blocks with environments that scale independently

## Environments on Demand

Ephemeral environments when set up the right way function independently of your infrastructure team or any one person in charge. They prevent blocks by allowing your team to collaborate asynchronously.

## Ephemeral environments and security

intrinsically, staging environments are not meant for wide audiences, regardless of best practices.

Don't share services or resources

SSO provider for auth logic

<natalie@shipyard.build> - Natalie Lumback

Ephemeral Environments 

Firewalls? Separate rules for ephemeral environments? So the devs can not play games with firewalls.

How do you manage the ephemeral environment management?
helm chart or docker compose for the environment

---  
