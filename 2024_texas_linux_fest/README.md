# Texas Linux Fest 2024

I had the opportunity to attend the 
[Texas Linux Fest](https://2024.texaslinuxfest.org/) conference held in Austin
Texas on  April 12-13 2024.

Texas Linux Fest is the first state-wide annual community-run conference for
Linux and open source software users and enthusiasts from around the Lone Star
State.

I took detailed notes on my personal computer for all the sessions that I
attended for reference and to reinforce learning.

The notes from the first day can be found in the [day1](day1) directory.

The notes from the second day can be found in the [day2](day2) directory.

The official slides can be found here.

The videos from the conference can be found here.

---  
