# Automated Container Updates with Github and CoreOs

Major Hayden from Red Hat

Why is he doing this talk?

His excitement around containers cannot be contained.

Make you excited about containers and CoreOS

Show how to keep container deployments updated for almost free

Point to resources for more learning

Do everything on a tiny budget

Worked in tech 20 years. Started pulling viruses off people's computers in neighborhood.

Own too many domain names. Porkbun TLD 

## What are containers?

Containers are a method for separating workloads on the same server

Using the same kernel as the host

Kept within CPU, memory, and other limits you specify

Shipped as images (tarballs)

Very fast when starting up

At the core of Kubernetes and OpenShift

Great for desktops, laptops, cloud instances and servers

Easily signed with cosign for integrity checking

Redis and valkey test them easily, not installed throw them away when done.

## Containers are NOT

Not virtual machines. May not be able to replace a VM with a container.

Some proprietary software works better in VMs. 

Not good for every application. Access to disk or something else.

## Containers usually have

A basic userspace from an OS

Additional OS package sor language-specific packages/modules

Your application

Initial startup instructions

Configuration does not go in the container. That is done later so you don't have to rebuild for config changes.

## Building containers

```
FROM docker.io/library/python:latest

COPY my_app.py /app

RUN pip install flask

ENTRYPOINT ["/app/my_app.py"]
```

WARNING: Always know where your container is coming from. You must trust and verify the source. 

Build it yourself

Kubernetes helps with running many containers and other resources along with all dependencies.

"If you have a problems with containers, try kubernetes. Then you have two problems."

## What is CoreOS?

The container optimized OS. A minimal OS with automatic updates. Scalable and secure.

CoreOS lets you focus heavily on your container workloads are always running.

Updates are atomic, much like an android phone. It is ready to run containers from the first boot.

Goodbye cloud-init. Hello ignition!

## Lets build stuff

The Plan:  

1. Deploy librespeed speed test server
2. Put the caddy webserver in front of it
3. Connect caddy to Porkbun to manage DNS records for certificates


Start with a very basic docker-compose.yml file

```
image: docker.io/adolfintel/speedtest
```

We are using the latest container.

Watchtower monitors for container repository for updates and keeps our containers updated locally.

See his watchtower block post for more details. https://major.io/

Watchtower optionally sends notifications for container updates too.

Webserver

Caddy gets TLS certificates for you automatically via HTTP or DNS validation

Caddy does not support Porkbun's DNS API, but there is a plugin for Porkbun.

Container file for Caddy

```
FROM caddy:2.7.6-builder AS builder
```

GitHub Actions workflow builds the container but how do we update to new caddy version when they are available?

Gets PRs automatically from Renovate for updates.

You can automerge these. Major is not using this yet. 

Add a new service for Caddy:

```
image: ghcr.io/major/caddy:main
```

Upper case Z tells SELinux this is the only container that can use it.
Lower case z tells SELinux that other containers can use the file.

Build a Caddy configuration file:

Place this in ./caddy/Caddyfile

On CoreOS just a few things

```
sudo systemctl enable --now docker.service

sudo usermod --append --groups docker core

loginctl enable-linger core
```

Lingering in systemd. If you log out systemd will kill your processes.

`docker-compose up --detach`

Rennovate watches for new caddy version and sends pull requests for updates.

Watchtower watches for container registry updates 

CoreOS can be deployed on several deployment choices.

## Final thoughts

Since you are updating containers automatically, ensure your container updates come from a reliable source! Dan Walsh talks about this all the time.

Upstream abandon projects and someone else registers that name.

Or build and host and containers yourself! Look up their dockerfile.

Add external monitoring or health checks via  docker-compose to know if an update caused a problem.

https://txlf24-containers.major.io

<major@mhtx.net>

---  
