## What is platform engineering

Evolution of devops

## Platform engineering components

Build & Development
Infrastructure provisioning
orchestration & runtimes

## Enabling and empowering users

From ideation to delivery
They build the machinery to enable people to do the things they do.

## Control planes

The foundational architecture for managing heterogeneous infrastructure.

Data plane, control plane

Kubernetes is a control plane for applications

Enablement for organizations.

## The universal control plane

What if we could create a control plane that can manage everything?
Enable every organization to be its own cloud provider

Strong API and extension system for building internal cloud platform.

CNCF Project, candidate for "graduation" this year

## Why?

* Architecture: core + providers and functions
* easy to define new platform APIs
* Automatically corrects drift
* Create desired state in any language
* Integration with CI/CD and other k8s tooling
* Day 2 operations , observability, workload identity

Prometheus metrics, time how long it takes to provision

Infrastructure engineering

MR = Managed Resources
xrd = composite resource definition

A managed resource is a kubernetes object that corresponds to a remote resource.

## Providers

The things that talk to the cloud providers

API server 

Kubernetes controller watches for changes in API server and provisions S3 controllers

## Composition

Building a product example   

Menu <- order  
Recipe -> meal

XRD <- claim  
Composition -> composite (XR)

CEL = Common Expression Language (in kubernetes 1.29)

## How do compositions work

Function engine, pipeline  
like a pipeline, take json in, mutate it, and spit out different json

## Function ecosystem is growing quickly

## Developer experience with Crossplane functions

```
crossplane beta trace
```

CRUD = Create Read Update Delete

---  
