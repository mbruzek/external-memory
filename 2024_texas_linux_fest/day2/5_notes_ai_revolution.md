# What the AI revolution means for Open Source 

Frank Karlitschek

Artificial Intelligence

Doing open source "forever". CEO and founder of NextCloud.

Google, Apple, Meta, Amazon, Windows

Motivation to build an alternative to these guys. They have too much information, own all our data and communication on the planet. Keep the IT under control.

With cloud our open source ideas are threatened. They use open source, but don't give back very much. Alternative to Microsoft 365.

Biggest cluster we are using is 20 million users.

## Nextcloud Hub

Files, Talk, Groupware, Office

Productivity, Security, On Premises, Open Source.

## NextCloud Files

Share with others, as you would expect

## NextCloud Talk

Chat and video conferencing. Native mobile apps, push notification. No bit is leaving  your machine if you don't want to.

## NextCloud Groupware

Calendar, mail client (need a mail server). Integrated directly with NextCloud. Encryption with S/MIME, and GPG. 

## NextCloud Office

Integration with Libre Office.

---  

Most feature complete alternative to Microsoft.

Life was good and then came AI.

Thought we could not compete anymore. How can a small open source company compete with the large companies. Do we even want to compete? Ethical challenges. 

Ethical discussions within the company. Shift to AI. Can we even do it? Built an AI team in NextCloud to explore our options.

Can not ignore AI. Useful tool. Can be a helper, or a friend. Useful features.

In a few years: We will not have to write some code.

Where does these recommendations come from? Is this in the spirit of open source?

Many companies are banning ChatGPT. 

* wrong information
* privacy and security - Take your data and store it for next iteration of AI. Ford could use AI but Chrysler would get a benefit of the competitor's data.

## Ethical AI

What is _ethical_ ai? Researched potential problems of AI. Non-discrimination. Machine learning models trained on the internet. The internet is not fair and balanced, full of racism and discrimination. Create a photo of a doctor. May get a white male.

Low C02 footprint. Large datacenters to train LLM. A bit of a problem. Not very transparent. They just do it. 

Privacy respecting. Confidential data in chatGPT systems, you don't know how they use that.

Freely available. Microsoft is charging money. Or account required. Open Source software is available to everyone. 

Created a traffic light system, rated from green to red.

Activate some of the features to see if you want to use the service or not.

Grading criteria:

1. Code must be open source. Measure the energy consumption
2. Model available
3. Training data available

If all three it is green, yellow for 2 and red for 0.

### Machine translation 

Machine translation can be implemented by different apps. OpenAI ChatGPT 3.5

Opus model by the University of Helsiniki.

### Speech to Text

Whisper and other online speech to text

Already in NextCloud:

* Facial recognition
* Object detection bicycle, dog, or something else.

## NextCloud Assistant 2

More advanced AI. 

* Uses LLM 
* 100% open source
* Fully self hosted

We have the resources to generate our own model. For the LLM we use existing foundation models. Lama and others. Companies must be transparent with their training data to avoid problems.

## NextCloud Assistant 

Integrated in the applications. 

## Assistant in Text

Markdown editor. Translate or make longer, or creates a headline, shorter. Mail composer, anywhere that is using text.

They do have an API that you can call for this.

## Assistant in Mail

Web mail client. Priority Inbox with important mail on top. Summary on email thread. (pretty cool and useful!). Compose a mail and ask it to write the mail.

Proof read before you send!

## Assistant in Talk

Translate messages

## AI image generator

Generate a photo or image as needed.

Latest version released in 2 weeks.

Summary bot. colleagues chatted while you were gone. Summarize chat threads.

Dictate text

Record call features will dictate telephone calls.

## Context Chat

Results are based on the model that was trained previously.

NextClout context chat, will train on your data.

---  

Depression is over. Hugging face is working on training data. The Open Source community has a chance. Ethical options that are getting better.

--- 

Q&A

Question: I am running NextCloud on a VM in my environment. What kind of horsepower do I need to run these stuff.

Depends. Text translation are pretty quick. Real LLM may take a few minutes. Separate server for the GPU double the resources. 

Question: Can we install this LLM component separately?

Yes. Over the last 1 1/2 year we redesigned NextCloud with microservices architecture. You can also use LLM as a service.

Question: Can we use these features outside of NextCloud via an API.

It works in 2 weeks when we launch version 8. 

Question: I run NextCloud at home, but don't want to have to enable this.

You can do it.

Question: Do you create your own LLM? 

Our mission and vision is to decentralize the Internet. We NextCloud will never run an LLM. Partnering with another companies. Otherwise we would be the central place for all this.

Question: Considering the Ethical questions. How do you rate yourself?

We are all green. But the companies that we partner with are yellow. If there is a good LLM with transparent training data. 

Question: How do you audit the energy consumption?

Energy depends on how much data you have, and how you use it. What is needed is transparency. Compare this with other companies that do the same. We can not measure it, we provide the service. Only half of the solution.

Question: Which vector database are you using?

A vector database is a relation database to figure out the tokens. Do not remember the name of it.  The LLMs have an abstraction layer so you can replace them. But the vector database is just picked for NextCloud.

We have to rely on LLM to get better so they have better context with long/short documents.

Germany is developing LLM for the public sector. Lama 2. 

In the future there will be different LLMs that know different things. One for schools that knows books, and one that knows how to fix motorbikes.

NextCloud is no longer just PHP. APIs are Python. Using Rust for other things. Co-pilot is not very good AI will not rewrite NextCloud very soon.

---  
