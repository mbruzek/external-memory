# Cloud Computing 101

Talia Nassi from Akamai lead developer advocate

https://buildwithtalia.com

From Basics to Building

Her job is to build things in the cloud. Test engineer, QA. Name dropping her past companies. Visa, WeWork, etc.

## Introduction to the cloud

What is the cloud? Graduated college studying computer science. Java script and python. No one taught her what a managed service. 

### The Client/Server mode

The client makes a request to the server, the server sends a response. 

Gmail example. All the emails stored in google's cloud.

Client is the computer requesting email. Server in Google Cloud for email.

Mobile devices, laptops, computers, tablets

Clients initiate requests for data, servers provide data. Could be a database, storage, networking.

Servers located in datacenters, multiple datacenters make up regions. Pick the region closest to the end user for performance reasons.

Set of servers that are maintained by a cloud provider. Manage and maintain the server, patching updates, and cooling.

1. Greater cost efficiency
  * Before the cloud: estimate resource requirements. Wrong estimates can be bad
  * After the cloud: pay as you go pricing. Only pay for what you need.

Time to market can be faster, focused on application and not infrastructure.

3. Greater Scalability and elasticity

4. Improved performance and security

Shared responsibility model. We will work with you on security. We will handle the hardware, you handle everything else.

## Types of cloud computing

On-premise. Servers you own.

Private cloud. Dedicated infrastructure. Mainly on site. You are responsible for infrastructure. Accessed over a VPN. Good for highly sensitive data. Full control over the servers. 

Public cloud. Shared on demand infrastructure that are hosted by third party companies. Akamai, Amazon, shared with different users/customers. Sharing resources with other organizations. Can get dedicated. 

Hybrid Cloud. Use both private and public clouds. Both are managed as one. Maintain control over some servers, but use shared resources to scale up to handle more volume. Black Friday. Memorial Day sale. Allows scale when needed. Highly configurable. 

Multi-Cloud. When you use more than one cloud provider. One for security, serverless. Benefits of each cloud provider's strength. Akamai is really good at security. Specializations of each cloud provider.

There is no one size fits all solution. Many cloud providers. Find the provider that works for your needs

## Building in the Cloud

Putting together resources that will make up your application. Resources vs Services. Connecting services that are composed of resources. S3 is a service, and it has S3 bucket. Linode has compute service, that is make up fo compute resources.

Resource type:  

1. Compute instance
    * Linode
    * Amazon Web Services
    * Google

2. Database - Collection of structured information stored on a server
    * MySQL
    * Redis
    * PostgreSQL
    * MongoDB
    * DynamoDB

Benefits of DB in the cloud. Ease of access. Scalability. Different type of databases. Disaster recovery. 

3. Storage solutions
    * Object storage - Files (objects) stored in flat data structure, buckets. Does not require use of a compute instance. Unique URL to access data. Publicly accessible or private. Images, documents, streaming assets. Scale these as well.  
    * Block storage - Add additional storage to compute instance. Enables you to store more data without resizing compute instance.  
    * Backups - Guard against accidental deletions or misconfigurations.  

4. Networking Solutions:
    * Firewalls - Filter the connections attempting to connect to the server. Prevent request from entering the network. 
    * Node balancers - Monitor how well the compute instances are handling traffic. Only send requests to healthy instances. 
    * Domain Name System (DNS) managers - Attach human readable name to an IP address. Find the name and address.

### Manage your resources

UI - Web console from cloud provider. Click to create resources from the web interface. This is not very fast.

Infrastructure as Code - Automate the process of provisioning. Create configuration files to create infrastructure. Build, change and manage your infrastructure in a safe, consistent manor.

* Ensures consistency between environments  
* Great for onboarding developers  
* Eliminates configuration drift  
* Decreases risk  

Making changes in production and not in staging is configuration drift.

Linode + Terraform

Terraform is open source and works with most cloud providers.

https://registry.terraform.io/

Cloud providers will publish their resources. Search for what you need. Paste into configuration files. Documentation on options. 

Full tutorial

https://tinyurl.com/2bkmxby7

$100 credit for Linode if you sign up.

Install Terraform using homebrew or "something like that".

Talked through the Terraform workshop. Rushing because she ran out of time.

---  
