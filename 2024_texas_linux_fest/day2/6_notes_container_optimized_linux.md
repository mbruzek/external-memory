# Container Optimized Linux

Kyle Davis

The best idea you're probably not using.

Was the developer advocate for the bottle rocket distribution. Taken a new role for the valkey project redis.

## What is a host OS and container optimized Linux?

Context: orchestrated cluster (k8s, ecs, etc). 

Interacting with the control plane. Bosses around the nodes. Data plane is where all your containers run. The containers are running on instances (VMs). 

Talking about the host OS that runs the containers. A very specialized type of distribution. 

Mission of a distribution (Ubuntu, Fedora, Arch) be as flexible as possible to run on different hardware. Variety of contexts. 

Specialized distributions DD-WRT, ChromiumOS, DoomLinux. Specialized hardware. DoomLinux is a distribution optimized to only run 1994 Doom. 

Container optimized Linux distributions. Bottlerocket, Flatcar, Talos. Very specific circumstances. 

Can Alpine run Doom. Yes. Can Ubuntu run Kubelet? Yes. 

Focus on one little thing. Slimmed down distro vs a general purpose distribution.  Remove components. Add configuration.

Think: Fast road car (mustang) Has breaks, engine, etc.

Container optimized Linux

Start from a clean sheet
Carefully add components (without mercy)
Integrated with orchestrators
Think: purpose-built race car

Bottlerocket and friends

It is a category not a single project

Bottlerocket as a representative

Every effort to represent others accurately but not an expert.

OG CoreOS RIP. Started this all. There is a modern CoreOS that is not the same.

Flatcar was a friendly fork by a company in Germany but now part of Microsoft.

Container-optimized OS (Google)

Kairos newer addition

Why not just use <distro> as a host OS?

* Mission mismatch - You are not trying to be flexible, trying to be inflexible on purpose.
* Unnecessary complexity 
* Difficult to secure & maintain

Typical Linux interactions

SSH -> shell -> package manager -> tools (interpreters)

SSH:  

* gate to castle
* common hole
* SSL is a big surface area

Shell:

* Exploitable by bad actors
* Encourages mutation - If everything is the same you fix it in the same way.

Package Manager:

* supply chain
* non-deterministic
* infinite surface area
* inconsistent interface

Interpreters:

* vulnerabilities
* memory hungry
* opening for exploits

## Clean sheet re-think

Immutable OS. Read only file system. No shell. API driven. No package manager. Use ready to run images. No SSH, manage through optional containers. 

## Containers: Heterogenous or homogeneous?

Install software to a general purpose linux to do different things, webserver, database, video.

A container optimized only needs an init system and and the kernel.

Then you run a base image to provide different services (webserver, database).

## What advantages do they bring?

### Security Profile

Container optimized can only host a container.

General purpose distro has to run other things ssh, package manager, writable file system. These are all potential security issues in a non-mission critical components.

Workloads don't really care what you are running on the base operating system. The host distro is abstracted away. Containers themselves have shells if necessary. 

General purpose distributions boot and mutate themselves with package manger update. Use of the system updates different mutations. These are now all different. They have drifted.

Bottlerocket boots an image boot and use, update is replace image operation.  All are the same. 

### Immutability

Immutable operating systems have mutable volumes for changes. Root file system is immutable. Partition A is running the root filesystem, partition B has the next image. Reboot swaps them.

### Exploring & Admin

If there is no shell how can we explore the system. Run a "host container". Log into the container and that has a file system mounted on the host system. You can turn off the container when not needed.

API - Change the max container log size, or change the DNS.

Traditionally get shell, edit some config file.  

Container optimized OS have API. Internal authenticated API. Manages all the changes to the configuration files. Gives a consistent interface. The API abstracts the details for the operator. You don't need to know which path or file to change.

## When you'd not want to use one.

When not to use a container optimized Linux distribution. 

* Deep host integrations
* Custom or specialized hardware - Raid drivers, etc
* Policy & security agents
* Homelab & tinkering

## Contact

https://bottlerocket.dev

https://flatcar.org

https://talos.dev

https://kairos.ios

CNCF slack: #wg-sp-os (working group special purpose operating systems)

<kyledvs@amazon.com>

## Q&A

Question: Can I use this at home on my kubernetes distribution. 

Bottlerocket is built in rust, takes 8 hours to build. Containerd cli is not documented very well
Flatcar would be better for your use case.

Question: Performance? Speed of boot?

Bottlerocket no benchmarks. People kept saying they boot so fast. Can skip entire steps in boot process. Image caching. Much faster. Not specifically optimized for performance.

Arm is well supported, but Risc5 is not supported.

Fedora CoreOS is the main community for CoreOS.  

Red Hat CoreOS runs on every OpenShift cluster.

--- 
