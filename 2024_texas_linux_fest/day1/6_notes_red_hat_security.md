# Red Hat Security workshop

Class registration

Richard Rios Associate Principal Solutions Architect
Jennifer Perry Senior Solutions Architect

Sorry for the bad releases of Satellite, it is better now.
Not sure if it will be released for RHEL 9, probably next release (10).

This course if for System administrators and architects.

Not fun when someone breaks into your systems.

## Terminal session recording

* Based on tlog

* https://red.ht/2pWcyni

* Other modes of session recording can still be used (auditd, script, sudo, etc)

## Why terminal session recording

You can use terminal session on a tablet when you get paged.

How is it different than auditd?
Configuration session recording with pam_tty_audit captures the data entered by the audited user on the command line. Output, error messages are missing from the connection fo session data.

Shows better on a web console.

Can session recording record GUI as well. No

yum install cockpit-session-recording

## Workshop

https://red.ht/43fCWSW

Short link to lab, using cockpit, enabling administrative access. Enable session recording for all sssd configs.

---  

# Configuring System-wide crypto policy

Introduced in RHEL 8.2

FIPS compliance is mandatory for government use.

## Complexity

Each crypto provider used its own configuration
* difficult and entirely manual process

```
update-crypto-policies --set
update-crypto-policies --show
```

Crypto policies

OpenSSL, GnuTLS, NSS, BIND, OpenJDK, etc.

**Can I define my own crypto policies?**
Yes as of RHEL 8.2.

This allows you to deploy policies across the organization.

Does setting FIPS crypto policy mean my instance is FIPS compliant?
No

https://red.ht/47ecHhc

Concepts included in this scenario:  

Verify the current system-wide cryptographic policy setting
Change the current cryptographic policy setting
Troubleshoot applications after a cryptographic policy update
Modify application settings to comply with stronger cryptographic policies

Example Use case:  

Your security team requires using stronger cryptography algorithms with applications and is no longer allowing weaker algorithms, such as SHA-1.

---

# Insights for RHEL

Workshop for Insights next. Many people leaving.

Insights https://red.ht/advisor-lab
rhel-a720/Redhat1!

Operations -> Drift -> Comparisons

Be able to compare systems, bios, os_release, 

Content -> Patch -> Systems

Can patch specific applications with tags.

Or patch at next maintenance window.

https://console.redhat.com/

Inventory -> Images

Create master images 

After completing this scenario, users will be able to register a system with Red Hat Insights and remediate issues that the Advisor tool has identified on the system.

Concepts included in this scenario:
* Register a system with Red Hat Insights  
* View Insights Advisor results that are reported for the system on cloud.redhat.com  
* Apply a recommended remediation for the system  
* Validate the remediation by updating system information  

Example use case:  

Administrators want to know about problematic configurations prior to experiencing problems on systems. Insights Advisor will look at data provided to the Advisor service and compare it against known problematic configurations and not only warn administrators through the service, but provide step-by-step remediation instructions and often times an Ansible playbook which can remediate the issue.

Feel free to try out other Insights included services like Vulnerability, Compliance, Patch, and more. We may add additional labs targeted to those Insights applications as well.

https://labs.redhat.com - free training for individuals
https://console.redhat.com - insights. All RH accounts have access.
https://developers.redhat.com 

---  
