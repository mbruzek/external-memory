# Bookmarks


## Coding

- [Bash documentation](https://tldp.org/LDP/abs/html/) - https://tldp.org/LDP/abs/html/
- [Greg's BashGuide](https://mywiki.wooledge.org/BashGuide) - https://mywiki.wooledge.org/BashGuide
- [Python documentation](https://docs.python.org/3/) - https://docs.python.org/3/
- [Seth Robertson's git choose-your-own-adventure](https://sethroberston.github.io/GitFixUm/fixup.html) - https://sethroberston.github.io/GitFixUm/fixup.html
- [Serial-to-WiFi Tutorial using ESP8266](http://fab.cba.mit.edu/classes/863.14/tutorials/Programming/serialwifi.html) - http://fab.cba.mit.edu/classes/863.14/tutorials/Programming/serialwifi.html
- [Subnet-Calculator](https://www.subnet-calculator.com/) - https://www.subnet-calculator.com/

---

## Interesting

- [Baudot Code - Decoder, Encoder, Converter, Translator](https://www.dcode.fr/baudot-code) - https://www.dcode.fr/baudot-code
- [Computer Aided Modeling (CAM) Linux](http://wiki.linuxcnc.org/cgi-bin/wiki.pl?Cam) - http://wiki.linuxcnc.org/cgi-bin/wiki.pl?Cam
- [Hacker Warehouse](https://hackerwarehouse.com/) - https://hackerwarehouse.com/
- [Johnson Space Center (JSC) Emergency Management](https://jscsos.com/) - https://jscsos.com/
- [makeCNC](https://www.makecnc.com/software/) - https://www.makecnc.com/software/
- [OpenDesk Design Playground](https://github.com/opendesk/design-playground) - https://github.com/opendesk/design-playground
- [Right Triangle Calculator](https://www.calculator.net/right-triangle-calculator.html) - https://www.calculator.net/right-triangle-calculator.html
- [Space Dash board](https://spacedashboard.com/) - https://spacedashboard.com/
- [Transposition Cipher](https://www.dcode.fr/transposition-cipher) - https://www.dcode.fr/transposition-cipher
- [Universal G-Code sender](https://winder.github.io/ugs_website/) - https://winder.github.io/ugs_website/

---

## Personal

- [BMO Requests](https://bmo.gd/requests-list) - https://bmo.gd/requests-list
- [Greater Houston Off Road Biking Association (ghorba)](http://ghorba.org/) - http://ghorba.org/

---

## Work

- [Jacobs CLG portal](https://portal.clg.jacobs.com/Pages/Home.aspx) - https://portal.clg.jacobs.com/Pages/Home.aspx
- [Jacobs JAMIS timecard system](https://newejamis.jacobstechnology.com/JET2/LogIn.aspx) - https://newejamis.jacobstechnology.com/JET2/LogIn.aspx
- [NASA SP-287 document](https://history.nasa.gov/SP-287/sp287.htm) - https://history.nasa.gov/SP-287/sp287.htm


## Research

- [Cloud Custodian Documentation](https://cloudcustodian.io/docs/index.html) - https://cloudcustodian.io/docs/index.html
- [CrowdStrike Reporting Tool for Azure (CRT)](https://github.com/CrowdStrike/CRT) - https://github.com/CrowdStrike/CRT
- [Debian Administration Handbook](https://debian-handbook.info/browse/stable/) - https://debian-handbook.info/browse/stable/
- [Debian Security](https://www.debian.org/doc/manuals/securing-debian-manual/index.en.html) - https://www.debian.org/doc/manuals/securing-debian-manual/index.en.html
- [Developer's Introduction to Containers](https://github.com/pnbrown/containers-intro) - https://github.com/pnbrown/containers-intro
- [Free Vectors for Laser Cutting](https://3axis.co/) - https://3axis.co/
- [HAWK Powershell Based tool for gathering information related intrusions and potential Breaches](https://github.com/T0pCyber/hawk) - https://github.com/T0pCyber/hawk
- [KubiScan: A tool to scan Kubernetes cluster for risky permissions](https://github.com/cyberark/KubiScan) - https://github.com/cyberark/KubiScan
- [Mandiant Azure AD Investigator](https://github.com/fireeye/Mandiant-Azure-AD-Investigator) - https://github.com/fireeye/Mandiant-Azure-AD-Investigator
- [Open datasets](https://daac.ornl.gov/) - https://daac.ornl.gov/
- [Pihole lists](https://firebog.net/) - https://firebog.net/
- [Sparrow is a tool to help detect possible compromised accounts and applications in the Azure/m365 environment](https://github.com/cisagov/Sparrow) - https://github.com/cisagov/Sparrow

---
