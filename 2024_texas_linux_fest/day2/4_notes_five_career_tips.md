# Five Tips For a Thriving Technology Career

Major Hayden from Red Hat

Using reveal.js for the first time. 

Mentor a lot of people. Asked the same question 

"How can I build a successful career in technology?"

Worked in tech to 20 years. Currently focused on RHEL in the public cloud.

Been down the technical and manager career tracks

Survived time working on security.

1. Align your work with you interests. Is not as simple as it sounds. 

Some jobs gives opportunities. Curiosity and interests. It is easier to be more engaged. Be open to new interests too. Something related to what you are working on. Watch out for passion. Passion and work do not go together, blind you to new alternatives. There are times where you don't seek out the work. The work finds you.

Security incident. Passwords changed at 1am when there was an outage. Piled around desk and yelled at person to fix it. 

Never write emails while you are angry. Save a draft and send it in the morning if you are still mad. 

CISO had a meeting with him at 7:30am next day. Lead a team of three that grew to six. Best and most stressful roles he has ever had. 

How did you survive this transition? Fake it until you make it. Always be open to learning. You won't be an expert on day one. 

Best opportunities broaden your interests. Be open to those!

2. Cut a path for others to follow

Tribal knowledge. Information locked up in someone who has been around a long time. Stratifies teams. Slow down development and testing. Limits career growth for people who have knowledge and who do not. 

Linux oracle. Knew everything about Linux. Give him a question and he would reply 1 or 2 things. Don't ask him the same question twice. Started writing down the questions and his answers and the questions others asked. Turned it into a wordpress blog where I documented problems or solutions.

https://major.io

Did you start the block with all the stuff people asked me? Thanks for doing that. Takes a weight off me. Customer was ultra demanding. 

Spreading knowledge feels good for for people who are and receive knowledge. Breaks down barriers.

Spread experience in the same way. Mentors use experience and knowledge to lift others up.

Mentor others and be mentored. Build a well-worn path for others to follow.

3. Speak up!

A wise manager asked me: What is the most valuable resource you can give someone? Time is your most valuable asset. Cannot be bought with more money. Is something that only you can protect. Suffering in silence while your time wastes away, is one of the worst thing on earth. Most of us do this fairly often.

Speak up to defend your time. Make sure your time is best utilized. Block time for meals, learning. Non work hours. For renewing documents. for those moments when you need a break to breathe. Block some time for walks. 

Handle meeting invitations with contempt. Just kidding, but not really.

Meeting invitation with no agenda. Ask for an agenda. What are we going to accomplish?

Meeting invitations with more than 3 people. Ask if you need to be present and what you are expected to provide. Delegate someone on your team to take your place. It gives opportunities.

Invitations have an an accept and decline button. Decline the invitation! 

Be efficient with asynchronous communication. Don't ask to ask, just ask. "Can I ask you a question?" 

Tell people as early as possible if something is blocking you from delivering on time. If you are unsure what you need to deliver. There is a chance you will deliver something late.

Protect your time.

4. Take care of yourself

Just like protecting your time. Protect your work/life balance. You are the only one that can defend your work/life balance. 

Major loves to read, finds most controversial topic and reads a book on that. Gives him new strategies at work. Ham radio. Mix technologies. 

His calendar has time blocked at lunch to run. Keeps him out of the kitchen.

Companies gives you time off for a reason. Be sure to use it! 

5. Build a path to "Yes".

Helps other people meet their goals. Ensures their changes meet your requirements. Makes others feel that you are vested in their success. 

Example: Working on a security team and a developer needs to deploy an application

However the application does not meet the company's security development requirements.

Instead of saying No find a way to say yes. Static analysis tool ahead of time. Give them requirements. Connect them with another team that solved the problem. 

Or make an exception for beta, for GA this will not work, needs to change.

Find middle ground, and work under the assumption that everyone wants the best.

X/Y problem

How do I get the last 3 characters of a filename? Do you mean the file extension like jpg or pdf? Yes I need the extension! OK good because not all extensions are 3 characters.

A path to "yes" does not mean becoming a yes-man. That is not helpful to anyone.

Remember the goal of why you come to work everyday. The customers.

Find a way to deliver the best possible experience for your customers by enableing others to do their best work.

Look for the win-win outcomes. How can I put this in the customers hands and be secure?

Think about the people behind you. 

I wrote a thing. Do you have documentation? Is there CI to make things better or worse?

https://txlf24-tech-career.major.io


---

## Q&A

Question: Senior people asking questions with the least context possible. Vague question with 5 words. How would you talk to an executive that asked you to do something without a lot of context.

What motivates them? How are they judged? I know how my boss's boss works or what he wants. I know you are judged on this. Help me understand what you need to make you successful. Do you want a plan of how to achieve this or do you want this. If you know you are having trouble being successful tell them that.

How to take your database of problem/solutions organization wide. How do you make that more of a cultural change? Encourage people to do this for tribal knowledge. 

It needs to be a 2 way street to get to the docs and contribute to the solution. Grass roots, from the ground up. Democratize access to the data. How much does this application make our company. Give them the % of revenue. So they know what they are working on.

Question: What is your best tip who has got away from tech roles, get back into hands dirty technical work? 

Be honest with the people around you. Fake it until you make it. I have been out of this for a while. Be humble. Rest comes along. Hard thing to do. 

Question: How do you determine if you are on the wrong track?

Not growing any more. Is it time management? Do I need to set different goals. Decline meetings? Forces other people to be self sufficient. Not growing. Or stressing about the act of work. 

---  
