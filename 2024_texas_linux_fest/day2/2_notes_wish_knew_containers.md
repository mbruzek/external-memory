# Things I Wish I Knew About Containers Sooner

Alex Juarez works for Red Hat

Who is this for?  Him. Three personas.

1. The container converts
2. The container curious
3. The container curmudgeons

Started in 2001 in IT and Technology.

## What do I hope you get from this?

* insight - a new way to think about something
* perspective - see where someone else might be
* Knowledge - learn something new

## How did we get to this topic?

Professional career 2007. Hosting. Dedicated machines/racks.
Until 2020. Training, mentoring. 

Part of a layoff sweep, after 13 years of employment.

Looking at job topics. Containers came up. Consider himself a life long learner.

Passed him by. Huge gap in knowledge. Wish someone had told me sooner.

## Where are we going?

* What is a Container
* Linux Technologies
* Working with Containers

Can not teach everything. Hopefully you can Google later.

Making technology work. Magic box. Things happen. Understand how it works on the system.

Doing linux for 20 years. But do not know containers.

## What are containers?

Containers are groups of processes running on a Linux system that are isolated from each other.

Video game cartridges. Lightweight, Self-contained, portable.

Everything is contained to run.

## Okay, But Why?

Containers allow you to run more things on a single host. More bang for buck.

VMs predate containers. Each VM had guest OS. Unnecessary duplication.

Remove the guest OS and duplication. Just the files you need to run the application.

NodeJs, Database, etc.

Higher density for a single host.

## Container Terminology

Container image = Base static image file. Example RHEL Universal Base Image (UBI). Immutable. Build application on top of a base image. 

Container engine - Software (Podman, Docker) for running containers on a single machine.

Container Orchestrators - Software for running containers across multiple machines (kubernetes, and swarm). Operating system for your data center. Resources CPU, RAM, Storage access.

## Linux Technologies

* Namespaces - kernel resource partitions. Isolation. `lsns`   
    `unshare --user --pid --map-root-user --mount-proc --fork bash`  

* CGroups - Control groups allow processes to be organized in hierarchical groups whose usage of various types of resources can be limited. Restriction. 

* SECCOMP - Secure Computing Mode (seccomp) is a kernel feature that allows you to filter systems calls to the kernel from a container. Disables 44 systems calls out of 300 available. Call made from userspace to kernel.

* SELinux - https://stopdisablingselinux.com/ learn to use it. Extended set of permissions. Who can access what file. Who can do what where. Default is to disable everything and enable access by profiles.

## Working with Containers

### Container engines and container runtimes

Docker and podman are popular container engines.

Interface with end-users. 

The technology was available for a long time. Docker made it easier to use in 2013.

Interface with image registries. Dockerhub, Quay

### Container runtime

Examples are runc and crun. No joke.

Manage the container life-cycle. 

Setup namespace, cgroup, manage storage, and network setup.

Run and manage the container

### Container Volumes

Containers are immutable, ephemeral. Volumes allow persistent storage.

Bind Mount - Allows one part of the file system to be mounted in other place in the file system. Example bind mount apache logs to user's home directory. Container could be mounted a configuration file or tools inside a container.

Provides persistent storage by decoupling file system storage from the container.

* provide files for web server
* share additional/updated config files
* test code changes

`podman run -d --rm -name ghost-app1 --network ghost-network --ip=10.89.0.10 -v /var/srv/containers/ghost-content:/var/lib/ghost/content:Z ghost`

The `-d` is for detach. The `-v` is volume mount. Local host directory to a directory in container. The `:Z` is for SELinux. 

Volumes can be re-used amongst containers. Stored as a file in container storage.

### Container Communication

Multiple containers, example wordpress, webserver, mysql server. 

* App/Host to container communication
* Container to container communication

```
podman image inspect docker.io/library/httpd:latest
"Config": {
    "ExposedPorts": {
        "80/tcp": {}
    }
}
```

Container to container communication they communicate if they are on the same network.

## Container Orchestration

Example:  

```
podman run -d --rm --name ghost-app1 --network ghost-network --ip=10.89.0.10 ghost
podman run -d --rm --name ghost-app2 --network ghost-network --ip=10.89.0.20 ghost
podman run -d --rm --name nginx --network ghost-network --ip=10.89.0.30 
```
Load balanced setup.

## In Review

1. Containers are not scary, just a game cartridge
2. CGroups, Namespaces, etc is JUST Linux!
3. Storage and Networking were really key to learn.

Demo.

--- 
